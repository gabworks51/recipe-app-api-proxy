# recipe-app-api-proxy

NGINX for recipe app configurations

## Usage

### Environment Variables

* `LISTEN_PORT` - port to listen on (default `8000`)
* `APP_HOST` - Hostname of the app to foward requests (default `app`)
* `APP_PORT` - Port of the app to forwards requests to (default `9000`)
